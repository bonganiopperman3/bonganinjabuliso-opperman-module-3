import 'package:flutter/material.dart';
import 'package:i_recommend/main.dart';

//4. This is a feature screen After Click the  Floating Button on Dashboard
class FeatureTwo extends StatelessWidget {
  const FeatureTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Module 3',
      theme: ThemeData(primarySwatch: Colors.yellow),
      home: Scaffold(
          appBar: AppBar(
        title: const Text("Feature Screen 1"),
      )),
    );
  }
}

class FeaturePage extends StatelessWidget {
  const FeaturePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const Center(child: Text('This is Screen 2')),
      bottomNavigationBar: FloatingActionButton(
          onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const HomePage()))
              }),
    );
  }
}
