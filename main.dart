import 'package:flutter/material.dart';
import 'package:i_recommend/DashBoard.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Module 3',
      theme: ThemeData(primarySwatch: Colors.yellow),
      home: const HomePage(),
    );
  }
}

//1. This code is for the LOGIN screen with username,password and button
class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: ElevatedButton(
        onPressed: () => {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const DashBoard()))
        },
        child: const Text('Login'),
        child: const TextField(
          decoration: InputDecoration(
              hintText: "Enter Username",
              labelText: "Username or Email",
              labelStyle: TextStyle(fontSize: 24, color: Colors.black),
              border: OutlineInputBorder()),
          keyboardType: TextInputType.emailAddress,
          obscureText: false,
        ),
      ),
      children: const [
        TextField(
          decoration: InputDecoration(
            hintText: "Change Password",
            labelText: "Password",
            labelStyle: TextStyle(fontSize: 24, color: Colors.black),
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.security),
          ),
          obscureText: true,
        ),
      ],
    );
  }
}
