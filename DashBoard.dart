// ignore: file_names
import 'package:flutter/material.dart';
import 'featureone.dart';

//2. This Code is for the Dashboard Screen after the Login
class DashBoard extends StatelessWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Module 3',
        theme: ThemeData(primarySwatch: Colors.yellow),
        home: Scaffold(
            appBar: AppBar(
          title: const Text("DashBoard"),
          actions: <Widget>[
            IconButton(
              onPressed: () => {},
              icon: const Icon(
                Icons.add_to_photos_rounded,
              ),
            ),
          ],
        )));
  }
}

class DashBoardContent extends StatelessWidget {
  const DashBoardContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const Center(child: Text('This is a Dashboard')),
      bottomNavigationBar: FloatingActionButton(
        onPressed: () => {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const FeatureOne()))
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
